import React from 'react';
import { slide as Menu } from 'react-burger-menu';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import MenuStyle from './MenuStyle';
const HeaderWrapper = styled.div`
  margin: 0 auto;
  padding: 0px;
  height: 50px;
`;
class Header extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <HeaderWrapper>
        <MenuStyle>
          <Menu styles={styles}>
            <Link to="/">Trang chủ</Link>
            <Link to="/users">Người dùng</Link>
          </Menu>
        </MenuStyle>
      </HeaderWrapper>
    );
  }
}

const styles = {
  bmMenu: {
    background: '#373a47',
    padding: '2.5em 1.5em 0',
    fontSize: '1.15em',
  },
  bmMorphShape: {
    fill: '#373a47',
  },
  bmItemList: {
    color: '#b8b7ad',
    padding: '0.8em',
  },
  bmOverlay: {
    background: 'rgba(0, 0, 0, 0.3)',
  },
};


export default Header;
