/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  errorLoadData: {
    id: 'tmv.components.Table.error',
    defaultMessage: 'There was an error! Please try again!',
  },
});
