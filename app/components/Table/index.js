import React from 'react';
import PropTypes from 'prop-types';
import { DataTable } from 'primereact/components/datatable/DataTable';
import { FormattedMessage } from 'react-intl';
import H1 from 'components/H1';
import messages from './messages';
import Wrapper from './Wrapper';
let dt = null;
export class Table extends React.Component {

  assignVal(et) {
    dt = et;
  }

  render() {
    if (this.props.isError) {
      return (<H1>
        <FormattedMessage {...messages.errorLoadData} />
      </H1>);
    }

    let pageLinkSize = 5;
    if (this.props.pageLinkSize) {
      pageLinkSize = this.props.pageLinkSize;
    }

    let paginatorPosition = 'bottom';
    if (this.props.paginatorPosition) {
      paginatorPosition = this.props.paginatorPosition;
    }

    let sortOrder = 1;
    if (this.props.sortOrder) {
      sortOrder = this.props.sortOrder;
    }

    let isLoading = false;
    if (this.props.isLoading) {
      isLoading = this.props.isLoading;
    }

    let totalItems = this.props.items.length;
    if (this.props.totalItems) {
      totalItems = this.props.totalItems;
    }

    return (
      <Wrapper>
        <DataTable
          ref={this.assignVal}
          value={this.props.items}
          header={this.props.header}
          footer={this.props.footer}
          selectionMode="single"
          selection={this.props.selectedItem}
          scrollable
          reorderableColumns={this.props.isReorderable}
          resizableColumns
          columnResizeMode="expand"
          rowsPerPageOptions={[10, 25, 50, 100]}
          paginator
          rows={this.props.rowsPerPage}
          totalRecords={totalItems}
          lazy
          onLazyLoad={this.props.onLazyLoad}
          pageLinkSize={pageLinkSize}
          exportFilename={this.props.exportFileName}
          loading={isLoading}
          paginatorPosition={paginatorPosition}
          sortField={this.props.sortProperty}
          sortOrder={sortOrder}
          contextMenu={this.props.contextMenu}      //filters={this.props.filters}
           onSelectionChange={this.props.onSelectionChange}
        // onColReorder={this.onColReorderVal}
        //onRowSelect={this.onUserSelect}
        //onPage={this.onPage}
        ///onFilter={this.onFilter}
        //onSort={this.onSort}
        >
          {this.props.columns}
        </DataTable>
      </Wrapper>
    );
  }
}
Table.propTypes = {
  isError: PropTypes.bool,
  items: PropTypes.array,
  exportFileName: PropTypes.string,
  header: PropTypes.object,
  footer: PropTypes.object,
  rowsPerPage: PropTypes.number,
  columns: PropTypes.array,
  isLoading: PropTypes.bool,
  isReorderable: PropTypes.bool,
  pageLinkSize: PropTypes.number,
  paginatorPosition: PropTypes.string,
  selectedItem: PropTypes.object,
  totalItems: PropTypes.number,
  sortProperty: PropTypes.string,
  sortOrder: PropTypes.number,
  onLazyLoad: PropTypes.func,
};

export default Table;

