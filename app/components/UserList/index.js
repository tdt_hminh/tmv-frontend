import React from 'react';
import PropTypes from 'prop-types';
import Hotkeys from 'react-hot-keys';
import moment from 'moment';
import { FormattedMessage } from 'react-intl';
import { Column } from 'primereact/components/column/Column';
import { InputText } from 'primereact/components/inputtext/InputText';
import { DataTable } from 'primereact/components/datatable/DataTable';
import { Dropdown } from 'primereact/components/dropdown/Dropdown';
import { Button } from 'primereact/components/button/Button';
import { ContextMenu } from 'primereact/components/contextmenu/ContextMenu';
import { Growl } from 'primereact/components/growl/Growl';
import { AddUser } from 'components/UserAdd';
import LoadingIndicator from 'components/LoadingIndicator';
import messages from './messages';

const columnList = [];
export class UsersList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      displayDialog: false,
    };
    this.onEmailFilterChange = this.onEmailFilterChange.bind(this);
  }

  onKeyDown() {
  }

  onEmailFilterChange(event) {
    this.dt.filter(event.value, 'email', 'contains');
    this.setState({ email: event.value });
  }

  initHeader() {
    const header =
      (<div>
        <div style={{ textAlign: 'left' }}>
          <i className="fa fa-search" style={{ margin: '4px 4px 0 0' }}></i>
          <InputText type="search" onInput={(e) => this.setState({ globalFilter: e.target.value })} placeholder="Search" size="50" />
        </div>
      </div>);
    return header;
  }

  initFooter() {
    const footer = (
      <div className="ui-helper-clearfix" style={{ width: '100%' }}>
        <Button
          style={{ float: 'left' }}
          icon="fa-plus"
          label="Add"
          onClick={() => this.addNew()}
        />
        <div style={{ textAlign: 'left' }}>
          <Button
            type="button"
            icon="fa-file-o"
            iconPos="left"
            label="Export Excel"
            onClick={() => this.exportExcel()}
          />
        </div>
      </div >);
    return footer;
  }

  exportExcel() {
    this.dt.exportCSV();
  }

  addNew() {
    this.setState({
      user: {
        username: '',
        email: '',
        displayName: '',
        source: 'site',
        isActive: 1,
        passwordHash: '123123',
        passwordSalt: 'salt',
        insertDate: moment().valueOf(),
        insertUserId: '1',
      },
      isAddNew: true,
      displayDialog: true,
    });
  }

  handleAdd(user) {
    this.setState({ displayDialog: false });
    this.props.handleAdd(user, this.state.isAddNew);
  }

  selectUser(selectedUser) {
    this.setState({
      displayDialog: true,
      isAddNew: false,
      user: Object.assign({}, selectedUser),
    });
  }

  generateColumns(fItem) {
    // Gen column names into list
    if (columnList && columnList.length > 0) return;
    if (fItem) {
      for (const key in fItem) {
        if (key) {
          const item = {
            field: key.toString(),
            header: key.toString(),
          };
          columnList.push(item);
        }
      }
    }
  }

  render() {
    if (this.props.error) {
      return (<p>
        <FormattedMessage {...messages.errorLoading} />
      </p>);
    }

    const header = this.initHeader();
    const footer = this.initFooter();
    /*     this.generateColumns(this.props.users[0]);
        const columns = columnList.map((col) => <Column key={col.field} field={col.field} header={col.header} filter filterMatchMode="contains" />);
        const totalItems = this.props.users.length;
     */
    const emails = [
      { label: 'All', value: null },
      { label: 'Gmail', value: 'gmail' },
      { label: 'Yahoo', value: 'yahoo' },
    ];

    const emailFilter = (
      <Dropdown
        style={{ width: '100%' }}
        className="ui-column-filter"
        value={this.state.email}
        options={emails}
        onChange={this.onEmailFilterChange}
      />);


    const items = [
      { label: 'View', icon: 'fa-search', command: () => this.selectUser(this.state.selectedUser) },
      { label: 'Delete', icon: 'fa-close', command: () => this.props.handleDelete(this.state.selectedUser) },
    ];


    return (
      <div className="content-section implementation">
        <Hotkeys
          keyName="shift+n,shift+e"
          onKeyDown={this.onKeyDown()}
        />

        <Growl ref={(el) => { this.growl = el; }}></Growl>

        <ContextMenu model={items} ref={(el) => this.cm = el} />

        <DataTable
          loading={this.props.loading}
          ref={(el) => this.dt = el}
          value={this.props.users}
          paginator
          rows={10}
          header={header}
          footer={footer}
          selectionMode="single"
          selection={this.state.selectedUser}
          onSelectionChange={(e) => { this.setState({ selectedUser: e.data, displayDialog: false }); }}
          resizableColumns
          columnResizeMode="expand"
          scrollable
          responsive
          reorderableColumns
          contextMenu={this.cm}
          globalFilter={this.state.globalFilter}
          rowsPerPageOptions={[10, 20, 40, 60]}
          totalRecords={this.props.totalRecords}
          lazy
          onLazyLoad={(evt) => this.props.onLazyLoad(evt)}
        >
          <Column
            field="userId"
            header="ID"
            style={{ width: '80px' }}
            sortable
            filter={false}
          />
          <Column
            field="username"
            header="Username"
            style={{ width: '200px' }}
            body={this.emailTemplate} // style
            sortable
            filter
            filterMatchMode="contains"
          />
          <Column
            field="displayName"
            style={{ width: '200px' }}
            header="FullName"
            sortable
            filter
            filterMatchMode="contains"
          />
          <Column
            field="email"
            header="Email"
            style={{ width: '300px' }}
            sortable
            filter
            filterElement={emailFilter}
          />
          <Column
            field="source"
            header="Source"
            style={{ width: '300px' }}
            sortable
            filter
            filterMatchMode="contains"
          />
          <Column
            field="insertUserId"
            header="insertUserId"
            style={{ width: '150px' }}
            sortable
            filter
            filterMatchMode="contains"
          />
          <Column
            field="updateUserId"
            header="updateUserId"
            style={{ width: '150px' }}
            sortable
            filter
            filterMatchMode="contains"
          />
          <Column
            field="updateDate"
            header="updateDate"
            style={{ width: '250px' }}
            sortable
            filter
            filterMatchMode="contains"
          />
          <Column
            field="passwordHash"
            header="Hash"
            style={{ width: '300px' }}
            sortable
            filter={false}
          />
          <Column
            field="passwordSalt"
            header="Salt"
            style={{ width: '300px' }}
            sortable
            filter={false}
          />
        </DataTable>

        <AddUser
          user={this.state.user}
          isAddNew={this.state.isAddNew}
          isDisplay={this.state.displayDialog}
          addNew={(user) => this.handleAdd(user)}
        />
      </div>
    );
  }
}

UsersList.propTypes = {
  loading: PropTypes.bool,
  totalRecords: PropTypes.number,
  error: PropTypes.any,
  users: PropTypes.any,
  onLazyLoad: PropTypes.func,
  handleAdd: PropTypes.func,
  handleDelete: PropTypes.func,
};

export default UsersList;
