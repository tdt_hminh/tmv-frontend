/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  errorLoading: {
    id: 'tmv.containers.UserPage.errorLoading',
    defaultMessage: 'Không thể lấy dữ liệu',
  },
});
