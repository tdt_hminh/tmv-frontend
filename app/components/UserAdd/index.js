import React from 'react';
import { Button } from 'primereact/components/button/Button';
import { Dialog } from 'primereact/components/dialog/Dialog';
import { InputText } from 'primereact/components/inputtext/InputText';
import PropTypes from 'prop-types';
import { Growl } from 'primereact/components/growl/Growl';

export class AddUser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      displayDialog: props.isDisplay,
      user: props.user,
      isAddNew: props.isAddNew,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.state = {
      displayDialog: nextProps.isDisplay,
      user: nextProps.user,
      isAddNew: nextProps.isAddNew,
    };
  }

  save() {
    this.props.addNew(this.state.user);
    this.close();
  }

  close() {
    this.setState({
      displayDialog: false,
    });
  }

  updateProperty(property, value) {
    const user = this.state.user;
    user[property] = value;
    this.setState({ user });
  }

  render() {
    const dialogFooter = (<div className="ui-dialog-buttonpane ui-helper-clearfix">
      <Button icon="fa-close" label="Close" onClick={() => this.close()} />
      <Button label="Save (Shift+S)" icon="fa-check" onClick={() => this.save()} />
    </div>);

    return (
      <div>
        <Growl ref={(el) => { this.growl = el; }}></Growl>

        <Dialog style={{ width: 600, height: 300 }} visible={this.state.displayDialog} header="User" modal footer={dialogFooter} onHide={() => this.setState({ displayDialog: false })}>
          {this.state.user && <div className="ui-grid ui-grid-responsive ui-fluid">
            <div className="ui-grid-row">
              <div className="ui-grid-col-4" style={{ padding: '4px 10px' }}><label htmlFor="username">Username</label></div>
              <div className="ui-grid-col-8" style={{ padding: '4px 10px' }}>
                <InputText autoFocus id="username" onChange={(e) => { this.updateProperty('username', e.target.value); }} value={this.state.user.username} />
              </div>
            </div>
            <div className="ui-grid-row">
              <div className="ui-grid-col-4" style={{ padding: '4px 10px' }}><label htmlFor="displayName">Display Name</label></div>
              <div className="ui-grid-col-8" style={{ padding: '4px 10px' }}>
                <InputText id="displayName" onChange={(e) => { this.updateProperty('displayName', e.target.value); }} value={this.state.user.displayName} />
              </div>
            </div>

            <div className="ui-grid-row">
              <div className="ui-grid-col-4" style={{ padding: '4px 10px' }}><label htmlFor="email">Email</label></div>
              <div className="ui-grid-col-8" style={{ padding: '4px 10px' }}>
                <InputText id="email" onChange={(e) => { this.updateProperty('email', e.target.value); }} value={this.state.user.email} />
              </div>
            </div>

          </div>}
        </Dialog>

      </div>
    );
  }
}
AddUser.propTypes = {
  isDisplay: PropTypes.bool,
  isAddNew: PropTypes.bool,
  user: PropTypes.any,
  addNew: PropTypes.func,
};

export default AddUser;

