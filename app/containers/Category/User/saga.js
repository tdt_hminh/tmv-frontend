import request from 'utils/request';
import { delay } from 'redux-saga';
import { call, put, takeLatest, all, takeEvery, race } from 'redux-saga/effects';
import { API_GET_USERS, LOAD_USERS, API_ADD_USER, ADD_USER, API_EDIT_USER, EDIT_USER, DELETE_USER, API_DELETE_USER } from './constants';
import { userLoaded, userLoadingError, userAdded, userAddError, userEdited, userEditError, userDeleted, userDeleteError } from './actions';

export function* getUsers(parameters) {
  let params;
  if (parameters === null) {
    params = {
      page: 0,
      rows: 10,
    };
  } else {
    params = parameters.params;
  }
  const page = params.page;
  const rows = params.rows;
  const direction = params.direction;
  const sortProperty = params.properties;
  let requestURL = `${API_GET_USERS}?page=${page}&size=${rows}`;
  if (sortProperty !== null) {
    requestURL += `&properties=${sortProperty}`;
  }
  if (direction !== null) {
    requestURL += `&direction=${direction}`;
  }
  console.log(requestURL);
  try {
    const data = yield call(request, requestURL);
    yield put(userLoaded(data));
  } catch (err) {
    yield put(userLoadingError(err));
  }
}

export function* addUser(data) {
  const requestURL = API_ADD_USER;
  try {
    yield call(request, requestURL, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(data.user),
    });
    yield put(userAdded());
    yield getUsers();
  } catch (err) {
    yield put(userAddError(err));
  }
}
export function* editUser(data) {
  const userId = data.user.userId;
  const requestURL = API_EDIT_USER + userId;

  try {
    yield call(request, requestURL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data.user),
    });
    yield put(userEdited());
    yield getUsers();
  } catch (err) {
    yield put(userEditError(err));
  }
}

export function* deleteUser(data) {
  const userId = data.user.userId;
  const requestURL = API_DELETE_USER + userId;

  try {
    yield call(request, requestURL, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data.user),
    });
    yield put(userDeleted());
    yield getUsers();
  } catch (err) {
    yield put(userDeleteError(err));
  }
}

export function* watchUsersData() {
  // Watches for LOAD_USERS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It will be cancelled automatically on component unmount
  yield takeLatest(LOAD_USERS, getUsers);
}

export function* watchUserAdd() {
  yield takeEvery(ADD_USER, addUser);
}

export function* watchUserEdit() {
  yield takeEvery(EDIT_USER, editUser);
}

export function* watchUserDelete() {
  yield takeEvery(DELETE_USER, deleteUser);
}

export default function* userSaga() {
  yield all([
    watchUsersData(),
    watchUserAdd(),
    watchUserEdit(),
    watchUserDelete(),
  ]);
}

