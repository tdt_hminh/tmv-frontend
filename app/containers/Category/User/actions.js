import {
  LOAD_USERS, LOAD_USERS_ERROR, LOAD_USERS_SUCCESS, ADD_USER, ADD_USER_ERROR, ADD_USER_SUCCESS,
  EDIT_USER_ERROR, EDIT_USER, EDIT_USER_SUCCESS,
  DELETE_USER, DELETE_USER_ERROR, DELETE_USER_SUCCESS,
} from './constants';

export function loadUsers(params) {
  return {
    type: LOAD_USERS,
    params,
  };
}

export function userLoaded(userData) {
  return {
    type: LOAD_USERS_SUCCESS,
    userData,
  };
}

/**
 * Dispatched when loading the repositories fails
 *
 * @param  {object} error The error
 *
 * @return {object}       An action object with a type of LOAD_USERS_ERROR  passing the error
 */
export function userLoadingError(error) {
  return {
    type: LOAD_USERS_ERROR,
    error,
  };
}

export function addUser(user) {
  return {
    type: ADD_USER,
    user,
  };
}

export function userAdded() {
  return {
    type: ADD_USER_SUCCESS,
  };
}

export function userAddError(error) {
  return {
    type: ADD_USER_ERROR,
    error,
  };
}

export function editUser(user) {
  return {
    type: EDIT_USER,
    user,
  };
}

export function userEdited() {
  return {
    type: EDIT_USER_SUCCESS,
  };
}

export function userEditError(error) {
  return {
    type: EDIT_USER_ERROR,
    error,
  };
}
export function deleteUser(user) {
  return {
    type: DELETE_USER,
    user,
  };
}

export function userDeleted() {
  return {
    type: DELETE_USER_SUCCESS,
  };
}

export function userDeleteError(error) {
  return {
    type: DELETE_USER_ERROR,
    error,
  };
}

