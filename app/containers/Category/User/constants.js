import { API_ENDPOINT } from 'containers/App/constants';

//export const API_ENDPOINT_MOCK = 'http://5af3eda504604e0014ea72b8.mockapi.io/api/';
export const API_GET_USERS = `${API_ENDPOINT}/user`;
export const API_ADD_USER = `${API_ENDPOINT}/user/`;
export const API_EDIT_USER = `${API_ENDPOINT}/user/`;
export const API_DELETE_USER = `${API_ENDPOINT}/user/`;
export const API_UPDATE_LIST_USERS = `${API_ENDPOINT}/user/list`;

export const LOAD_USERS = 'tmv/category/User/LOAD_USER';
export const LOAD_USERS_SUCCESS = 'tmv/category/User/LOAD_SUCCESS';
export const LOAD_USERS_ERROR = 'tmv/category/User/LOAD_ERROR';

export const ADD_USER = 'tmv/category/User/ADD_USER';
export const ADD_USER_SUCCESS = 'tmv/category/User/ADD_SUCCESS';
export const ADD_USER_ERROR = 'tmv/category/User/ADD_ERROR';

export const EDIT_USER = 'tmv/category/User/EDIT_USER';
export const EDIT_USER_SUCCESS = 'tmv/category/User/EDIT_USER_SUCCESS';
export const EDIT_USER_ERROR = 'tmv/category/User/EDIT_USER_ERROR';

export const DELETE_USER = 'tmv/category/User/DELETE_USER';
export const DELETE_USER_SUCCESS = 'tmv/category/User/DELETE_USER_SUCCESS';
export const DELETE_USER_ERROR = 'tmv/category/User/DELETE_USER_ERROR';
