import React from 'react';
import PropTypes from 'prop-types';
import H1 from 'components/H1';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { Growl } from 'primereact/components/growl/Growl';
import { makeSelectError } from 'containers/App/selectors';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import UsersList from 'components/UserList';

import messages from './messages';
import reducer from './reducer';
import saga from './saga';
import { loadUsers, addUser, editUser, deleteUser } from './actions';
import { makeSelectUsers, makeActionUserStatus, makeSelectTotalRecords, makeSelectLoading } from './selectors';

export class UserPage extends React.Component {
  onLazyLoad(event) {
    console.log(event);
    const first = event.first;
    const rows = event.rows;

    const page = first / rows;
    const properties = event.sortField;
    const direction = event.sortOrder;
    const params = {
      page,
      rows,
      direction,
      properties,
    };
    this.props.loadUsers(params);
  }

  handleAdd(user, isAdd) {
    if (isAdd) {
      this.props.addNew(user);
    } else {
      this.props.edit(user);
    }
  }

  render() {
    const { loading, error, users, totalRecords, addSuccess } = this.props;

    const usersListProps = {
      loading,
      error,
      users,
      addSuccess,
      totalRecords,
    };

    return (
      <div>
        <Helmet>
          <title>User</title>
          <meta name="description" content="User" />
        </Helmet>
        <H1>
          {/* <FormattedMessage {...messages.header} /> */}
        </H1>
        <Growl ref={(el) => this.growl = el} />

        <UsersList
          {...usersListProps}
          onLazyLoad={(evt) => this.onLazyLoad(evt)}
          handleAdd={(user, isAdd) => this.handleAdd(user, isAdd)}
          handleDelete={(user) => this.props.delete(user)}
        />
      </div>
    );
  }
}
UserPage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
  users: PropTypes.array,
  totalRecords: PropTypes.number,
  loadUsers: PropTypes.func,
  addNew: PropTypes.func,
  edit: PropTypes.func,
  delete: PropTypes.func,
  addSuccess: PropTypes.bool,
};

export function mapDispatchToProps(dispatch) {
  return {
    loadUsers: (params) => dispatch(loadUsers(params)),
    addNew: (user) => dispatch(addUser(user)),
    edit: (user) => dispatch(editUser(user)),
    delete: (user) => dispatch(deleteUser(user)),
  };
}

const mapStateToProps = createStructuredSelector({
  users: makeSelectUsers(),
  totalRecords: makeSelectTotalRecords(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
  addSuccess: makeActionUserStatus(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'user', reducer });
const withSaga = injectSaga({ key: 'user', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(UserPage);
