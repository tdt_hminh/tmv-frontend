import { fromJS } from 'immutable';

import {
  LOAD_USERS,
  LOAD_USERS_SUCCESS,
  LOAD_USERS_ERROR,
  ADD_USER,
  ADD_USER_ERROR,
  ADD_USER_SUCCESS,
} from './constants';

const initialState = fromJS({
  loading: false,
  error: false,
  userData: {
    totalRecords: 0,
  },
});

function userReducer(state = initialState, action) {
  console.log('reducer: ' + action.type);
  switch (action.type) {
    case LOAD_USERS:
      return state
        .set('loading', true)
        .set('error', false)
        .setIn(['userData', 'totalRecords'], 0)
        ;
    case LOAD_USERS_SUCCESS:
      return state
        .setIn(['userData', 'users'], action.userData.data.content)
        .setIn(['userData', 'totalRecords'], action.userData.data.totalElements)
        .set('loading', false);
    case LOAD_USERS_ERROR:
      return state
        .set('error', action.error)
        .set('loading', false);
    case ADD_USER:
      return state
        .set('loading', true)
        .set('error', false);
    case ADD_USER_SUCCESS:
      return state
        .set('loading', false)
        .set('add_success', true);
    case ADD_USER_ERROR:
      return state
        .set('error', action.error)
        .set('loading', false)
        .set('add_success', false);
    default:
      return state;
  }
}

export default userReducer;

