import { createSelector } from 'reselect';


const selectUser = (state) => state.get('user');

const makeSelectUsers = () => createSelector(
  selectUser,
  (userState) => userState.getIn(['userData', 'users'])
);
const makeSelectTotalRecords = () => createSelector(
  selectUser,
  (userState) => userState.getIn(['userData', 'totalRecords'])
);

const makeActionUserStatus = () => createSelector(
  selectUser,
  (userState) => userState.get('add_success')
);

const makeSelectLoading = () => createSelector(
  selectUser,
  (userState) => userState.get('loading')
);

export {
  selectUser,
  makeSelectUsers,
  makeActionUserStatus,
  makeSelectTotalRecords,
  makeSelectLoading,
};
