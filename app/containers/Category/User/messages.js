/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'tmv.containers.UserPage.header',
    defaultMessage: 'Danh mục người dùng',
  },
  errorLoading: {
    id: 'tmv.containers.UserPage.errorLoading',
    defaultMessage: 'Không thể lấy dữ liệu',
  },
  title: {
    id: 'tmv.containers.UserPage.title',
    defaultMessage: 'Người dùng',
  },
});
