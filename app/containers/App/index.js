import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';

import HomePage from 'containers/HomePage/Loadable';
import FeaturePage from 'containers/FeaturePage/Loadable';
import UserPage from 'containers/Category/User/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Header from 'components/Header';
import Footer from 'components/Footer';

import 'font-awesome/css/font-awesome.css';
import 'primereact/resources/primereact.min.css';
import 'primereact/resources/themes/kasper/theme.css';
import 'sass/App.scss';

const AppWrapper = styled.div`
  margin: 0 auto;
  padding: 0px;
`;
const AppContent = styled.div`
  margin: 0 auto;
  padding: 80px;
`;

export default function App() {
  return (
    <AppWrapper>
      <Helmet
        titleTemplate="%s - TMV"
        defaultTitle="TMV"
      >
        <meta name="description" content="TMV" />
      </Helmet>
      <Header />
      <AppContent>
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/features" component={FeaturePage} />
          <Route path="/users" component={UserPage} />
          <Route path="" component={NotFoundPage} />
        </Switch>
        <Footer />
      </AppContent>
    </AppWrapper>
  );
}
